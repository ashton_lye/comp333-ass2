/*
 * Constructor function for a ShareWidget instance.
 * 
 * container_element : a DOM element inside which the widget will place its UI
 *
 */
 
function ShareWidget(container_element){
	
	//declare variables for the data properties of the widget
	var selectedShares = [];

	//an object literal representing the widget's UI
	var _ui = {   

		container: null,
		//titlebar contains the selector and update buttons
		titlebar: null,
		selector: null,
		button: null,
		//sortbar holds the radio buttons and labels to choose the sorting
		sortbar: null,
		radio1: null,
		radio2: null,
		//list is the container the ShareLine objects are appended to
		list: null,
	};
	
	/*
	Constructor function for an inner object to hold the full share data for a company
	*/
	var ShareLine = function(s_company, s_price, s_change){
		
		//an object literal representing the UI for the share info
		var _ui = {				
			
			//declare variables for the UI properties of the ShareLine
			//container holds the share data labels
			container: null,
			//labels display the share data
			nameLabel: null,
			priceLabel: null,
			changeLabel: null,
		};
		
		/* 
		function to create the DOM elements needed for the ShareLine UI
		*/
		var _createUI = function(){
	
			//create and initialise each of the UI elements and add them to the
			//_ui object
			//creating container
			_ui.container = document.createElement("div");
			//creating name label and text, applying css inline so no css file necessary
			_ui.nameLabel = document.createElement("p");
			_ui.nametext = document.createTextNode(s_company);
			_ui.nameLabel.appendChild(_ui.nametext);
			_ui.nameLabel.setAttribute("style","float:left; display: inline-block;");

			//creating price label and text, applying css inline so no css file necessary
			_ui.priceLabel = document.createElement("p");
			_ui.pricetext = document.createTextNode(s_price);
			_ui.priceLabel.appendChild(_ui.pricetext);
			_ui.priceLabel.setAttribute("style","padding-left:25%; padding-right:25%; display: inline-block;");

			//creating change label and text, applying css inline so no css file necessary
			_ui.changeLabel = document.createElement("p");
			_ui.changetext = document.createTextNode(s_change);
			_ui.changeLabel.appendChild(_ui.changetext);
			_ui.changeLabel.setAttribute("style","float:right; display: inline-block;");

			//appending labels to share container
			_ui.container.appendChild(_ui.nameLabel);
			_ui.container.appendChild(_ui.priceLabel);
			_ui.container.appendChild(_ui.changeLabel);

		};
		
		//function to append share container to the list container in the ShareWidget ui
		this.appendFunction = function() {
			list = document.getElementById(container_element.id+"list");
			list.appendChild(_ui.container);	
		};
	
		_createUI();    //call this function last to build the UI
	 
	};  
	//End of ShareLine constructor function
	
	
	//private method to construct the DOM subtree for the UI and put into container element
	var _createUI = function(){
		//set container for entire widget - the container in the HTML - and applying css
		_ui.container = container_element;
		_ui.container.setAttribute("style","width:30%;");

		//creating the elements within the titlebar - a text label, the selector and the update button
		_ui.titlebar = document.createElement("div");
		_ui.titlebar.setAttribute("style", "background-color: #0d47a1;");
		
		_ui.titlebar.label = document.createElement("p");
		_ui.titlebar.text = document.createTextNode("Select Company: ");
		_ui.titlebar.label.setAttribute("style", "color: #FFFFFF; display: inline-block;")
		_ui.titlebar.label.appendChild(_ui.titlebar.text);

		_ui.selector = document.createElement("select");
		_ui.selector.setAttribute("style", "width: auto");

		_ui.button = document.createElement("button");
		_ui.buttontext = document.createTextNode("Update");
		_ui.button.appendChild(_ui.buttontext);

		//appending the titlebar elements to the titlebar and then appending titlebar to the container
		_ui.container.appendChild(_ui.titlebar);
		_ui.titlebar.appendChild(_ui.titlebar.label);
		_ui.titlebar.appendChild(_ui.selector);		
		_ui.titlebar.appendChild(_ui.button);

		//creating sortbar and it's elements - labels, radio buttons and the ShareLine container
		_ui.sortbar = document.createElement("div");
		_ui.sortbar.setAttribute("style", "background-color: #1976d2;"); 

		_ui.list = document.createElement("div");
		_ui.list.setAttribute("style", "background-color: #1e88e5;");
		_ui.list.setAttribute("id", container_element.id+"list");

		_ui.sortbar.label = document.createTextNode("Sort By: ");
		_ui.radio1 = document.createElement("input");
		_ui.radio1.setAttribute("type", "radio");
		_ui.radio1.setAttribute("value", "company");
		_ui.sortbar.companyLabel = document.createTextNode("Company");

		_ui.radio2 = document.createElement("input");
		_ui.radio2.setAttribute("type", "radio");
		_ui.radio2.setAttribute("value", "price");
		_ui.sortbar.priceLabel = document.createTextNode("Price");

		//appending sortbar and elements to the container
		_ui.container.appendChild(_ui.sortbar);
		_ui.sortbar.appendChild(_ui.sortbar.label);
		_ui.sortbar.appendChild(_ui.radio1);
		_ui.sortbar.appendChild(_ui.sortbar.companyLabel);
		_ui.sortbar.appendChild(_ui.radio2);
		_ui.sortbar.appendChild(_ui.sortbar.priceLabel);
		_ui.sortbar.appendChild(_ui.list);

		//creating the option elements for the company selector
		//Would have preferred to do it from the database but the assessment specifies a fixed list and I ran out of time
		_ui.titlebar.option1 = document.createElement("OPTION");
		_ui.titlebar.option1.setAttribute("value","NZ Wind Farms");
		_ui.titlebar.option1text = document.createTextNode("NZ Wind Farms");
		_ui.titlebar.option1.appendChild(_ui.titlebar.option1text);

		_ui.titlebar.option2 = document.createElement("OPTION");
		_ui.titlebar.option2.setAttribute("value","Foley Wines");
		_ui.titlebar.option2text = document.createTextNode("Foley Wines");
		_ui.titlebar.option2.appendChild(_ui.titlebar.option2text);

		_ui.titlebar.option3 = document.createElement("OPTION");
		_ui.titlebar.option3.setAttribute("value","Geneva Finance");
		_ui.titlebar.option3text = document.createTextNode("Geneva Finance");
		_ui.titlebar.option3.appendChild(_ui.titlebar.option3text);

		_ui.titlebar.option4 = document.createElement("OPTION");
		_ui.titlebar.option4.setAttribute("value","Xero Live");
		_ui.titlebar.option4text = document.createTextNode("Xero Live");
		_ui.titlebar.option4.appendChild(_ui.titlebar.option4text);

		_ui.titlebar.option5 = document.createElement("OPTION");
		_ui.titlebar.option5.setAttribute("value","Moa Group Ltd");
		_ui.titlebar.option5text = document.createTextNode("Moa Group Ltd");
		_ui.titlebar.option5.appendChild(_ui.titlebar.option5text);

		_ui.titlebar.option6 = document.createElement("OPTION");
		_ui.titlebar.option6.setAttribute("value","Solution Dynamics");
		_ui.titlebar.option6text = document.createTextNode("Solution Dynamics");
		_ui.titlebar.option6.appendChild(_ui.titlebar.option6text);

		//appending selector options
		_ui.selector.appendChild(_ui.titlebar.option1);
		_ui.selector.appendChild(_ui.titlebar.option2);
		_ui.selector.appendChild(_ui.titlebar.option3);
		_ui.selector.appendChild(_ui.titlebar.option4);
		_ui.selector.appendChild(_ui.titlebar.option5);
		_ui.selector.appendChild(_ui.titlebar.option6);

		//function to make stuff happen when a company is selected
		_ui.selector.onchange = function(){
			getShareInfo(_ui.selector.value);
		};

		//This was going to be the update function but it doesn't work, I ran out of time to fix/finish it
		//Left it in for posterity, along with the updateShares.php file, which is probably terrible because I only sort of knew what I was doing
		// _ui.button.onclick = function(){
		// 	var jsonShares = JSON.stringify(selectedShares);
		// 	ajaxRequest("POST", "updateShares.php", true, "json="+jsonShares, function(response){
		// 		console.log(JSON.stringify(response));
		// 	});
		// };

		//function to make stuff happen when radio 1 is clicked - call sorting function and make sure radio 2 is unchecked
		//sorting doesn't work properly
		_ui.radio1.onclick = function() {
			_ui.radio2.checked = false;
			selectedShares.sort(companyComparator);
			displayShareInfo();
			//console.log(selectedShares);
		};

		//function to make stuff happen when radio 2 is clicked - call sorting function and make sure radio 1 is unchecked
		//sorting doesn't work properly
		_ui.radio2.onclick = function() {
			_ui.radio1.checked = false;
			selectedShares.sort(priceComparator);
			displayShareInfo();
			//console.log(selectedShares);
		};

	};

	//ajax function - might look familiar cuz I copied it from assessment 1
	var ajaxRequest = function(method, url, async, data, callback){
		var request = new XMLHttpRequest();
		request.open(method,url,async);
		
		if(method == "POST"){
			request.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		}
		
		request.onreadystatechange = function(){
			if (request.readyState == 4) {
				if (request.status == 200) {
					var response = request.responseText;
					callback(response);
				} else {
					alert(request.statusText);
				}
			}
		}

		request.send(data);
	}
	 
	//performing ajax request to get share data for the selected company and adding the returned data to an array of displayed companies
	var getShareInfo = function(value) {
		//console.log(value);
		ajaxRequest("POST", "getShares.php", true, "company="+value, function(response) {
			parsedResponse = JSON.parse(response);
			length = selectedShares.length;
			//console.log("length:" + length);
			//console.log("response:" + parsedResponse[0].company);

			//checking if the company is already being displayed. If it is, don't add it's data to the array.
			//in theory anyway. I just noticed it doesn't work for anything other the first displayed.
			//suspect it's something to do with the breaks, but I don't have time to fix it now

			//automatically add to the array if it's empty
			if (length == 0) {
				selectedShares.push(parsedResponse);
			} else {
				for (var i = 0; i < length; i++) {
					//console.log("selectedShares:" + selectedShares[i][0].company);
					//console.log(i);
					if (parsedResponse[0].company != selectedShares[i][0].company) {
						selectedShares.push(parsedResponse);
						break;
					} else if (parsedResponse[0].company == selectedShares [i][0].company) {
						alert("already displayed!");	
						break;
					};
				};
			};
			//console.log(selectedShares);
			displayShareInfo();
		});
	}

	//actually displaying the share info
	var displayShareInfo = function() {
		//console.log(container_element.id)
		list = document.getElementById(container_element.id+"list");
		//remove existing displayed shares
		while (list.hasChildNodes()) {  
			list.removeChild(list.firstChild);
		};
		//iterate through the array creating ShareLine objects for each company and appending them.
		for (var i = 0; i < selectedShares.length; i++) {
			shareDisplay = new ShareLine(selectedShares[i][0].company, selectedShares[i][0].price, selectedShares[i][0].change);
			shareDisplay.appendFunction();
		};
	};

	//sorting by company, not quite working currently
	function companyComparator(a, b) {
		for (var i = 0; i < selectedShares.length; i++) { 
			if (selectedShares[i][0].company < selectedShares[i+1][0].company) return -1;
			if (selectedShares[i][0].company > selectedShares[i+1][0].company) return 1;
			return 0;
		};
	};

	//sorting by price, not working at all currently
	function priceComparator(a, b) {
		for (var i = 0; i < selectedShares.length; i++) {
			var a = selectedShares[i][0].price;
			var b = selectedShares[i+1][0].price;
			//console.log(a, b);
			if (a < b) {
				return -1;
			};
			if (a > b) {
				return 1;
			};
			return 0;
		};
	};	
	 
	 /**
	  * private method to intialise the widget's UI on start up
	  */
	var _initialise = function(container_element){
		_createUI(container_element);

		}
	  	
	_initialise(container_element);   //finally call the _initialise function 
}
	 

	 
	 