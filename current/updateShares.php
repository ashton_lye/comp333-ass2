<?php
    $received = $_POST['json'];

    require_once('connect.php');

    $selectedShares = json_decode($received);
    $arr = [];

    for ($x = 0; $x <= count($selectedShares); $x++) {
        $company = $selectedShares[x][0]->company;
        $query = "SELECT * FROM shareprices WHERE Name = ".$company;
        $result = $con->query($query);
        while ($row = $result->fetch()) {
            $arr[] = array (
                "company" => $row ['Name'],
                "price" => $row['Price'],
                "change" => $row['Change'],
            );
        }
    }
    $json = json_encode($arr);

    echo $json;
?>