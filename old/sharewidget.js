/*
 * Constructor function for a ShareWidget instance.
 * 
 * container_element : a DOM element inside which the widget will place its UI
 *
 */
 
function ShareWidget(container_element){
	
	//declare variables for the data properties of the widget
	var _this_widget_instance = this;
	
	
	var _ui = {     //an object literal representing the widget's UI
		
		//declare variables for the UI properties of the widget
		topDiv = function() {
			var div = document.createElement("div");
			div.id = "sharesTop";
		},
		bottomDiv = function() {
			var div = document.createElement("div");
			div.id = "sharesBottom";
		},
		selectLabel = function() {
			var label = document.createElement("p");
			p.innerHTML = "Select Company: ";
		},
		companySelector = function() {
			var selector = document.createElement("select");
			selector.id = "companies";
		},
		updateButton = function() {
			var button = document.createElement("button");
			button.id = "update";
			button.innerHTML = "Update";
			button.onclick = function () {
				updateShares();
			}
		}
	};
	
	/**
	* Constructor function for an inner object to hold the full share data for a company
	*/

	var ShareLine = function(s_company, s_price, s_change){
		
		//declare variables for the data properties of one company's share information
		var Name = s_company;
		var Price = s_price;
		var Change = s_change;
	
		var _ui = {					//an object literal representing the UI for the share info
			
			//declare variables for the UI properties of the ShareLine

		};
		
		/* function to create the DOM elements needed for the ShareLine UI
		*/
		var _createUI = function(){
				
			//create and initialise each of the UI elements and add them to the
			//_ui object

		};
		

	//getter methods for ShareLine should be defined next as needed 

	
		_createUI();    //call this function last to build the UI
	 
  };  //End of ShareLine constructor function
	
	
	//getter methods for ShareWidget should be defined next as needed 

	
	//private method to construct the DOM subtree for the UI and put into container element
	var _createUI = function(){
		/*container_element.innerHTML += '<div style="background-color:#FF0000" id="sharesTop"></div>';
		container_element.innerHTML += '<div style="background-color:#00FF00" id="sharesBottom"></div>';

		var sharesTop = document.getElementById("sharesTop");
		sharesTop.innerHTML += '<h3>Share Monitor</h3>';
		sharesTop.innerHTML += '<p>Select Company:</p>';
		sharesTop.innerHTML += '<select id="companies"></select><br>';
		sharesTop.innerHTML += '<button id="update" onclick="Javascript:updateShares()">Update</button>';*/

		container_element.innerHTML += new _ui.topDiv;
		container_element.innerHTML += new _ui.bottomDiv;
		_ui.topDiv.innerHTML += new _ui.selectLabel;
		_ui.topDiv.innerHTML += new _ui.companySelector;
		_ui.topDiv.innerHTML += new _ui.updateButton;


		var companyList = document.getElementById("companies");
		ajaxRequest("GET", "getCompanies.php", true,'', function(response) {
			var data = JSON.parse(response);
			for (var i = 0; i < data.length; i++) {
				companyList.innerHTML += '<option value="'+data[i].name+'">'+data[i].name+'</option>"';
			};
		});

		//var sharesBottom = document.getElementById("sharesBottom");
		sharesBottom.innerHTML += '<p>Sort By: </p>';
		sharesBottom.innerHTML += '<p> Company</p><input type="radio" name="sort" value="company" checked>';
		sharesBottom.innerHTML += '<p> Price</p><input type="radio" name="sort" value="price">';
		sharesBottom.innerHTML += '<div style="background-color:#0000FF" id="shares"></div>';
		

		//radioForm.innerHTML += '<input type="radio" name="company" value="company" checked> <input type="radio" name="price" value="price">';
		//radioForm.innerHTML += '<input type="radio" name="price" value="price">';
		//create the DOM elements needed for the widget and add to the _ui var
		
		
	};
	

	 
	
	/**
	 * private methods for the rest of the functionality should
	 * be added below 
	 */

	
	/**
	* private method to intialise the widget's UI on start up
	*/
	var _initialise = function(container_element){
		_createUI(container_element);

	} 	
	_initialise(container_element);   //finally call the _initialise function 
}

var shareArray = [];

function ajaxRequest(method, url, async, data, callback){

	var request = new XMLHttpRequest();
	request.open(method,url,async);
	
	if(method == "POST"){
		request.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	}
	
	request.onreadystatechange = function(){
		if (request.readyState == 4) {
			if (request.status == 200) {
				var response = request.responseText;
				callback(response);
			} else {
				alert(request.statusText);
			}
		}
	}

	request.send(data);
}; 

function updateShares() {
	alert("updateShares run");
	var company = companies.value;
	console.log(company);
	ajaxRequest("POST", "updateShares.php", true, "company="+company, function(response) {
		alert(response);
		var shareDisplay = document.getElementById("shares");
		shareDisplay.innerHTML += '<p>' + response + '</p><br>';
	});
};

	 
	 