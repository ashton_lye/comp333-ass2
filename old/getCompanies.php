<?php
    require_once('connect.php');

    $query = "SELECT * FROM shareprices";
    $arr = [];

    $result = $con->query($query);

    while ($row = $result->fetch()) {
        $arr[] = array (
            "name" => $row ['Name'],
        );
    };

    $json = json_encode($arr);
    echo $json;
?>